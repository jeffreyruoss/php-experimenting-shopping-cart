<?php

/* --- =show POST, GET, COOKIE and SESSION arrays for development --- */
$dev_tool_bar_enabled = false; // set to true or false


/* --- =set domain/root dir of the site --- */
// $root = 'http://jruoss.userworld.com/php/14/'; // orielly domain
$root = 'http://kittenstockphotos.com'; // my localhost domain


/* --- =DEVELOPMENT - show POST, GET, COOKIE and SESSION arrays --- */
function dev_tool_bar($dev_tool_bar_enabled) {
	if ($dev_tool_bar_enabled) {
		echo '<div style="overflow: hidden;">';

		echo '<div style="width: 25%; float:left;">POST:<pre>';
		print_r($_POST);
		echo '</pre><br><br></div>';

		echo '<div style="width: 25%; float:left;">GET:<pre>';
		print_r($_GET);
		echo '</pre><br><br></div>';

		echo '<div style="width: 25%; float:left;">COOKIE:<pre>';
		print_r($_COOKIE);
		echo '</pre><br><br></div>';

		echo '<div style="width: 25%; float:left;">SESSION:<pre>';
		print_r($_SESSION);
		echo '</pre><br><br></div>';

		echo '</div>';
	}
}


/* --- =max 8 - number of products to be displayed --- */
$num_products = 8;


/* --- =Set product names and prices here --- */
$product_data = array(
	array('name' => 'Product #1', 'price' => 10),
	array('name' => 'Product #2', 'price' => 12),
	array('name' => 'Product #3', 'price' => 15),
	array('name' => 'Product #4', 'price' => 8),
	array('name' => 'Product #5', 'price' => 10),
	array('name' => 'Product #6', 'price' => 5),
	array('name' => 'Product #7', 'price' => 10),
	array('name' => 'Product #8', 'price' => 6)
);