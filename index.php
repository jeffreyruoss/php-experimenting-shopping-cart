<?php

session_set_cookie_params(0);
session_start();
include('config.php');
include('functions.php');

dev_tool_bar($dev_tool_bar_enabled);


?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>KittenStock Photos</title>
	<link rel="stylesheet" href="style.css">
</head>
<body>
	<div class="top-bar">
		<?php signed_in_as(); ?>
		<ul>

			<li><a href="<?php echo $root; ?>#" class="home">Home</a></li>

			<?php if (has_products()) { ?>
			<li><a href="<?php echo $root; ?>?view_cart_check_out=true" class="view-cart-check-out"><span class="fontawesome-shopping-cart"> </span><span class="cart-number"> <?php echo product_count(); ?> </span> View Cart / Check Out</a></li>
			<?php } ?>

			<?php if (!is_logged_in()) { ?>
			<li><a href="<?php echo $root; ?>?sign_in=true" class="sign-in">Sign In</a></li>
			<li><a href="<?php echo $root; ?>?sign_up=true" class="sign-up">Sign Up</a></li>
			<?php } ?>
		</ul>
	</div>
	<div class="header">
		<h1 class="site-title"><a href="<?php echo $root; ?>#">KittenStock Photos</a></h1>
		<p class="tagline">Quality Kitten Images from Top Photographers</p>
	</div>
	<div class="wrap">
		<div class="message-area">
			<?php
				if($_GET['added_to_cart_message']) {
					$product = urldecode($_GET['product']);
					echo '<p><span class="fontawesome-ok"></span> ' . $product . ' has been added to your cart!</p>';
				}
				else if ($_GET['sign_up_thanks_message']) {
					echo '<p><span class="fontawesome-thumbs-up"></span> Thank you for signing up!</p>';
				}
				else if ($_GET['log_in_message']) {
					echo '<p><span class="fontawesome-ok"></span> You are now logged in.</p>';
				}
				else if ($_GET['log_out_message']) {
					echo '<p>You have logged out.</p>';
				}
				else if ($_GET['check_out_success']) {
					echo '<p><span class="fontawesome-thumbs-up"></span> Thank you for your purchase!</p>';
					echo '<p class="small-text">You will receive a confirmation email shortly.</p>';
				}
				else if ($_GET['sign_up_or_sign_in_to_buy']) {
					$sign_up = '<a href="' . $root . '?sign_up=true">Sign Up</a>';
					$sign_in = '<a href="' . $root . '?sign_in=true">Sign In</a>';
					echo '<p>Please ' . $sign_up . ' or ' . $sign_in . ' to make a purchase</p>';
				}
			?>
		</div>
		<div class="main-content-area">
		<?php 
			if ($_GET['sign_up']) {
				sign_up();
			}
			else if ($_GET['sign_up_submit']) {
				process_sign_up_form();
			}
			else if ($_GET['log_out']) {
				log_out();
			}
			else if ($_GET['sign_in']) {
				sign_in();
			}
			else if ($_GET['sign_in_submit']) {
				process_sign_in_form();
			}
			else if ($_GET['buy']) {
				buy_product();
			}
			else if ($_GET['view_cart_check_out']) {
				view_cart_check_out($product_data);
			}
			else if ($_GET['check_out_submit']) {
				check_out_submit($product_data);
			}
			else if ($_GET['remove_item']) {
				remove_item($_GET['remove_item']);
			}
			else if ($_GET['increment_quantity']) {
				quantity_increment($_GET['increment_quantity'], $_GET['product_key']);
			}
			else {
				generate_products($product_data, $num_products);
			}
		?>
		</div>
	</div>
</body>
</html>

<?php
?>

