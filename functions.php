<?php

/* --- =check if someone is logged in --- */
function is_logged_in() {
	if ($_COOKIE[$_SESSION['logged_in_name']]) {
		return true;
	}
	return false;
}


/* --- =get currently logged in username --- */
function get_username() {
	return $_COOKIE[$_SESSION['logged_in_name']];
}


/* --- =check if there are any products in cart --- */
function has_products() {
	if(is_logged_in()) {
		$username = get_username();
		$item_prefix = $username . '_cart_item_';
		foreach ($_COOKIE as $key => $value) {
			if (strpos($key, $item_prefix) !== false) {
				return true;
			}
		}
		return false;
	}
}


/* --- =Sign Up Area --- */
function sign_up() {
	?>
	<div class="error-container">
		<?php
			if ($_GET['name'] == 'blank') {
				$name = $_GET['name'] == '';
				$name_class = 'error';
				echo '<p>Username cannot be blank</p>';
			}
			else if ($_GET['error_name_exists'] == true) {
				$name = $_GET['error_name_exists'];
				$name_class = 'error';
				echo '<p>This name already exists.</p>';
			}
			else if ($_GET['error_name_invalid'] == true) {
				$name = $_GET['error_name_invalid'];
				$name_class = 'error';
				echo '<p>Username cannot have spaces or special characters.</p>';
			}
			else {
				$name = $_GET['name'];
			}
			if ($_GET['email'] == 'blank') {
				$email = $_GET['email'] == '';
				$email_class = 'error';
				echo '<p>Email cannot be blank</p>';
			}
			else if ($_GET['error_email_exists'] == true) {
				$email = $_GET['error_email_exists'];
				$email_class = 'error';
				echo '<p>This email already exists.</p>';
			}
			else if ($_GET['error_email_invalid'] == true) {
				$email = $_GET['error_email_invalid'];
				$email_class = 'error';
				echo '<p>This is not a valid email address';
			}
			else {
				$email = $_GET['email'];
			}
		?>
	</div>
	<div class="sign-up-in-form-container">
		<h2>Sign Up</h2>
		<form action="<?php echo $root; ?>?sign_up_submit=true" method="POST" class="sign-up-form">
			<label for="Name">Username</label>
			<input class="<?php echo $name_class; ?>" type="text" name="name" value="<?php echo $name; ?>">
			<label for="Email">Email</label>
			<input class="<?php echo $email_class; ?>" type="text" name="email" value="<?php echo $email; ?>">
			<button type="submit">SUBMIT</button>
		</form>
	</div>
	<?php
}


/* --- =Process sign up form - create user --- */
function process_sign_up_form() {
	$error = false;
	if ($_POST['name'] == '') {
		$name_get_string = '&name=blank';
		$error = true;
	}
	else if (already_exists($_POST['name'])) {
		$name_get_string = '&error_name_exists=' . $_POST['name'];
		$error = true;
	}
	else if (!valid_name($_POST['name'])) {
		$name_get_string = '&error_name_invalid=' . $_POST['name'];
		$error = true;
	}
	else {
		$name_get_string = '&name=' . $_POST['name'];
	}
	if ($_POST['email'] == '') {
		$email_get_string = '&email=blank';
		$error = true;
	}
	else if (already_exists($_POST['email'])) {
		$email_get_string = '&error_email_exists=' . $_POST['email'];
		$error = true;
	}
	else if (!valid_email($_POST['email'])) {
		$email_get_string = '&error_email_invalid=' . $_POST['email'];
		$error = true;
	}
	else {
		$email_get_string = '&email=' . $_POST['email'];
	}
	if ($error) { // if any errors
		$location_string = 'Location: ' . $root . '?sign_up=true' . $name_get_string . $email_get_string;
		header($location_string); // redirect back to the form
	}
	else {
		$next_login_id = increment_login_ids();
		$name_key = 'name_' . $next_login_id;
		$email_key = 'email_' . $next_login_id;

		$_SESSION['logged_in_name'] = $name_key;
		$_SESSION['logged_in_email'] = $email_key;

		setcookie($name_key, $_POST['name'], time()+3600*24);
		setcookie($email_key, $_POST['email'], time()+3600*24);

		$get_val = '?sign_up_thanks_message=true';
		$name = 'name=' . $_POST['name'];
		$location_string = 'Location: ' . $root . $get_val . '&' . $name;
		header($location_string);
	}
}


/* --- =increment login ids for multiple registered users creation --- */
function increment_login_ids() {
	$next_id = 1;
	foreach ($_COOKIE as $key => $value) {
		$name_prefix = 'name_';
		if (strpos($key, $name_prefix) !== false) {
			$current_cookie_id = substr($key, strpos($key, '_') + 1);
			if ($current_cookie_id == $next_id) {
				$next_id++;
			}
		}
	}
	return $next_id;
}


/* --- =valid name --- */
function valid_name($name) {
	if (strpos($name, ' ') !== false) { // if it has a space
		return false;
	}
	else if (preg_match('/[^a-z_\-0-9]/i', $name)) { // if it has a special character
		return false;
	}
	return true;
}

/* --- =valid email address --- */
function valid_email($email) {
	return filter_var($email, FILTER_VALIDATE_EMAIL);
}


/* --- =check if a value already exists in the cookies (example: name or email) --- */
function already_exists($value_to_check) {
	foreach ($_COOKIE as $key => $value) {
		if ($value == $value_to_check) {
			return true;
		}
	}
}


/* --- =Check if signed in and ouput signed in as markup --- */
function signed_in_as() {
	if(is_logged_in()) {
		?>
		<p class="signed-in-as">
			You are logged in as: <br>
			<?php	echo get_username() . ' - ' . $_COOKIE[$_SESSION['logged_in_email']]; ?>
			<br><a href="<?php echo $root; ?>?log_out=true" class="log_out">log out</a>
		</p>
		<?php
	}
}


/* --- =Log out --- */
function log_out() {
	session_destroy();
	$location_string = 'Location: ' . $root . '?log_out_message=true';
	header($location_string);
}


/* --- =Sign In Area --- */
function sign_in() {
	?>
		<div class="error-container">
			<?php
				if ($_GET['error_wrong_login']) {
					echo '<p>The login you have entered is incorrect.</p>';
				}
			?>
		</div>	
		<div class="sign-up-in-form-container">
			<h2>Sign In</h2>
			<form action="<?php echo $root; ?>?sign_in_submit=true" method="POST" class="sign-in-form">
				<label for="Name">Username</label>
				<input type="text" name="name">
				<label for="Email">Email</label>
				<input type="text" name="email">
				<button type="submit">SUBMIT</button>
			</form>
		</div>
		<p class="dont-have-account">
			Don't have an account yet? <a href="<?php echo $root; ?>?sign_up=true">Sign Up</a>
		</p>
	<?php
}


/* --- =Process sign in form --- */
function process_sign_in_form() {
	if (already_exists($_POST['name']) && already_exists($_POST['email'])) {
		$_SESSION['logged_in_name'] = array_search($_POST['name'], $_COOKIE);
		$_SESSION['logged_in_email'] = array_search($_POST['email'], $_COOKIE);
		$location_string = 'Location: ' . $root . '?log_in_message=true';
		header($location_string);
	}
	else {
		$location_string = 'Location: ' . $root . '?sign_in=true&error_wrong_login=true';
		header($location_string);
		?>
		<?php
	}
}


/* --- =This is for generating different kitten images from placekitten.com --- */
function generate_dimensions_array($num_products) {
	$result = array();
	$base_dimension = 400;
	for ($i=0; $i < $num_products; $i++) { 
		$result[] = $base_dimension;
		$base_dimension++;
	}
	return $result;
}


/* --- =Generate the products markup --- */
function generate_products($product_data, $num_products) {
	if ($num_products > 8) {
		$num_products = 8;
	}
	$dimensions_array = generate_dimensions_array($num_products); 
	?>
	<div class="main-products-container">
	<?php
	for ($i=0; $i < $num_products; $i++) {
		$dimensions = $dimensions_array[$i];
		?>
			<div class="product-container">
				<img src="http://placekitten.com/<?php echo $dimensions; ?>" alt="">
				<h3 class="product-title"><?php echo $product_data[$i]['name']; ?></h3>
				<p class="description">
					Lay down in your way biting sleep on your face bat jump on the table, catnip eat hairball kittens catnip lay down in your way.
				</p>
				<p class="price">$<?php echo $product_data[$i]['price']; ?></p>
				<a href="<?php echo $root; ?>?buy=true&product=<?php echo urlencode($product_data[$i]['name']); ?>"><button>BUY</button></a>
			</div>
	<?php } ?>
	</div>
	<?php
}


/* --- =buy product - when a buy button is clicked --- */
function buy_product() {
	if (is_logged_in()) {
		$item = urldecode($_GET['product']);
		$already_in_cart = already_in_cart($item); // return the cookie value of this product that is already in cart (or return false)
		if ($already_in_cart) {
			$item_key = array_search($already_in_cart, $_COOKIE);
			$item_with_quantity = increase_quantity($already_in_cart);
		}
		else {
			$item_key = get_username() . '_cart_item_' . increment_cart_item_ids();
			$item_with_quantity = $item . ' x1';
		}
		setcookie($item_key, $item_with_quantity, time()+3600*24);
		$location_string = 'Location: ' . $root . '?added_to_cart_message=true&product=' . urlencode($item);
		header($location_string);
	}
	else {
		$location_string = 'Location: ' . $root . '?sign_up_or_sign_in_to_buy=true';
		header($location_string);
	}
}


/* --- =check if already in cart --- */
function already_in_cart($item) {
	$username = get_username();
	$item_prefix = $username . '_cart_item_';
	foreach ($_COOKIE as $key => $value) { 
		if (strpos($key, $item_prefix) !== false) {
			$base_value = substr($value, 0, -3); // remove quantity suffix
			if ($item == $base_value) { // this one is a duplicate
				return $_COOKIE[$key];
			}
		}
	}
	return false;
}


/* --- =increase quantity of a product in it's cookie value --- */
function increase_quantity($cart_item) {
	$num = substr($cart_item, -1); // get the qualtity number (last character)
	$num = (int)$num; // cast to an integer
	$num++; // increase by one
	$cart_item = substr_replace($cart_item, $num, -1); // replace with new quantity number
	return $cart_item;
}


/* --- =increase quantity of a product in it's cookie value --- */
function decrease_quantity($cart_item) {
	$num = substr($cart_item, -1); // get the qualtity number (last character)
	$num = (int)$num; // cast to an integer
	$num--; // decrease by one
	$cart_item = substr_replace($cart_item, $num, -1); // replace with new quantity number
	return $cart_item;
}


/* --- =increment cart item ids --- */
function increment_cart_item_ids() {
	$next_id = 1;
	$username = get_username();
	$item_prefix = $username . '_cart_item_';
	foreach ($_COOKIE as $key => $value) {
		if (strpos($key, $item_prefix) !== false) {
			$current_cookie_id = substr($key, -1);
			if ($current_cookie_id == $next_id) {
				$next_id++;
			}
		}
	}
	return $next_id;
}


/* --- =product count for the view cart button--- */
function product_count() {
	if(is_logged_in()) {
		$username = get_username();
		$item_prefix = $username . '_cart_item_';
		$count = 0;
		foreach ($_COOKIE as $key => $value) {
			if (strpos($key, $item_prefix) !== false) {
				$num = substr($value, -1); // get the qualtity number (last character)
				$num = (int)$num; // cast to an integer
				$count = $count + $num;
			}
		}
		return $count;	
	}
}


/* --- =view cart check out area --- */
function view_cart_check_out($product_data) {
	?>
		<div class="error-container">
			<?php
				if ($_GET['name'] == 'blank') {
					$name_class = 'error';
					echo '<p>Name field cannot be blank</p>';
				}
				else {
					$name = $_GET['name'];
				}
				if ($_GET['email'] == 'blank') {
					$email_class = 'error';
					echo '<p>Email field cannot be blank</p>';
				}
				else if ($_GET['error_email_invalid'] == true) {
					$email = $_GET['error_email_invalid'];
					$email_class = 'error';
					echo '<p>This is not a valid email address</p>';
				}
				else {
					$email = $_GET['email'];
				}
				if ($_GET['address'] == 'blank') {
					$address_class = 'error';
					echo '<p>Address field cannot be blank</p>';
				}
				else {
					$address = $_GET['address'];
				}
				if ($_GET['ccnumber'] == 'blank') {
					$ccnumber_class = 'error';
					echo '<p>Credit Card Number field cannot be blank</p>';
				}
				else if ($_GET['error_ccnumber_invalid'] == true) {
					$ccnumber = $_GET['error_ccnumber_invalid'];
					$ccnumber_class = 'error';
					echo '<p>This is not a valid credit card number</p>';
				}
				else {
					$ccnumber = $_GET['ccnumber'];
				}
			?>
		</div>
		<div class="view-cart-check-out-wrap">
			<div class="view-cart-container">
				<h2>Your Cart</h2>
				<table>
					<tr>
						<th class="x-col"></th>
						<th>Product</th>
						<th class="qty-col">QTY</th>
						<th>Price</th>
					</tr>
					<?php list_cart_items($product_data); ?>
				</table>
			</div>
			<div class="check-out-container">
				<h4>Billing Information</h4>
				<form action="<?php echo $root; ?>?check_out_submit=true" method="POST">
					<label for="Name">Name</label>
					<input class="<?php echo $name_class; ?>" type="text" name="name" value="<?php echo $name; ?>">
					<label for="Email">Email</label>
					<input class="<?php echo $email_class; ?>" type="text" name="email" value="<?php echo $email; ?>">
					<label for="Address">Address</label>
					<input class="<?php echo $address_class; ?>" type="text" name="address" value="<?php echo $address; ?>">
					<label for="Credit Card Number">Credit Card Number</label>
					<input class="<?php echo $ccnumber_class; ?>" type="text" name="ccnumber" value="<?php echo $ccnumber; ?>">
					<button type="submit">SUBMIT</button>
				</form>
			</div>
		</div>
	<?php
}


/* --- =check out submit --- */
function check_out_submit($product_data) {
	$error = false;
	if ($_POST['name'] == '') {
		$name_get_string = '&name=blank';
		$error = true;
	}
	else {
		$name_get_string = '&name=' . $_POST['name'];
	}
	if ($_POST['email'] == '') {
		$email_get_string = '&email=blank';
		$error = true;
	}
	else if (!valid_email($_POST['email'])) {
		$email_get_string = '&error_email_invalid=' . $_POST['email'];
		$error = true;
	}
	else {
		$email_get_string = '&email=' . $_POST['email'];
	}
	if ($_POST['address'] == '') {
		$address_get_string = '&address=blank';
		$error = true;
	}
	else {
		$address_get_string = '&address=' . $_POST['address'];
	}
	if ($_POST['ccnumber'] == '') {
		$ccnumber_get_string = '&ccnumber=blank';
		$error = true;
	}
	else if (!valid_ccnumber($_POST['ccnumber'])) {
		$ccnumber_get_string = '&error_ccnumber_invalid=' . $_POST['ccnumber'];
		$error = true;
	}
	else {
		$ccnumber_get_string = '&ccnumber=' . $_POST['ccnumber'];
	}

	if ($error) { // if any errors
		$location_string = 'Location: ' . $root . '?view_cart_check_out=true' . $name_get_string . $email_get_string . $address_get_string . $ccnumber_get_string;
		header($location_string); // redirect back to the view cart check out area
	}
	else {
		$location_string = 'Location: ' . $root . '?check_out_success=true';
		send_verification_email($product_data);
		clear_cart();
		header($location_string);
	}
}

/* --- =clear cart cookies after checkout --- */
function clear_cart() {
	if(is_logged_in()) {
		$username = get_username();
		$item_prefix = $username . '_cart_item_';
		foreach ($_COOKIE as $key => $value) {
			if (strpos($key, $item_prefix) !== false) {
				setcookie($key, '', -1);
			}
		}
	}
}

/* --- =send verification email --- */
function send_verification_email($product_data) {
	$to = $_POST['email'];
	$from = 'KittenStock Photos';
	$email_subject = 'KittenStock Purchase Confirmation';

	$headers = "From: " . $from . "\r\n";
	$headers .= 'MIME-Version: 1.0' . "\n";
	$headers .= 'Content-type: text/html; carset=iso-8859-1' . "\r\n";

	$email_template = 'email_template.txt';
	$email_content = file_get_contents($email_template);

	$name = $_POST['name'];
	$email = $_POST['email'];
	$address = $_POST['address'];
	$ccnumber = ccnumber_mask($_POST['ccnumber']);

	$email_content = str_replace('#NAME#', $name, $email_content);
	$email_content = str_replace('#EMAIL#', $email, $email_content);
	$email_content = str_replace('#ADDRESS#', $address, $email_content);
	$email_content = str_replace('#CCNUMBER#', $ccnumber, $email_content);

	$email_content = list_cart_items_receipt($product_data, $email_content);

	mail($to, $email_subject, $email_content, $headers);
}

/* --- =x out first 12 digits of credit card number --- */
function ccnumber_mask($ccnumber) {
	$result = str_replace('-', '', $ccnumber); // remove hypens
	$result = substr($result, 12); // get last 4
	$result = 'XXXX-XXXX-XXXX-' . $result; // prepend Xs
	return $result;
}


/* --- =valid credit card number --- */
function valid_ccnumber($ccnumber) {
	$ccnumber = str_replace('-','', $ccnumber); // remove hypens
	if (is_numeric($ccnumber) && strlen($ccnumber) == 16) { // check if it 16 digits
		return true;
	}
	else {
		return false;
	}
}


/* --- =list cart items in a table--- */
function list_cart_items($product_data) {
	if(is_logged_in()) {
		$username = get_username();
		$item_prefix = $username . '_cart_item_';
		$total = 0;
		foreach ($_COOKIE as $key => $value) {
			if (strpos($key, $item_prefix) !== false) {
				$quantity = substr($value, -1); // get the qualtity number (last character)
				$quantity = (int)$quantity; // cast to an integer				
				$value = substr($value, 0, -3); // remove quantity suffix		
				$price = get_item_price($product_data, $value);
				$price *= $quantity;
				$total = $total + $price;
				?>
					<tr>
						<td class="x-col"><a href="<?php echo $root . '?remove_item=' . $key; ?>"><span class="fontawesome-remove-sign"></span></a></td>
						<td><?php echo $value; ?></td>
						<td class="qty-col"><?php echo quantity_increment_ui($quantity, $key); ?></td>
						<td>$<?php echo $price; ?></td>
					</tr>
				<?php
			}
		}
		?>
			<tr>
				<td class="x-col"></td>
				<td class="total-label">Total</td>
				<td class="qty-col"></td>
				<td class="total-value">$<?php echo $total; ?></td>
			</tr>
		<?php
	}
}


/* --- =quantity incrementer --- */
function quantity_increment_ui($quantity, $key) {
	?>
		<a href="<?php echo $root . '?increment_quantity=add&product_key=' . $key; ?>">
			<span class="fontawesome-caret-up"></span>
		</a>
		<a class="number">
			<?php echo $quantity; ?>
		</a>
		<a href="<?php echo $root . '?increment_quantity=subtract&product_key=' . $key; ?>">
			<span class="fontawesome-caret-down"></span>
		</a>
	<?php
}


/* --- =quantity_increment --- */
function quantity_increment($direction, $product_key) {
	if ($direction == 'add') {
		$new_value = increase_quantity($_COOKIE[$product_key]);
		setcookie($product_key, $new_value, time()+3600*24);
	}
	else if (substr($_COOKIE[$product_key], -1) == '1') { // if qty is 1
		setcookie($product_key, null, -1); // remove item cookie from cart
	}
	else {
		$new_value = decrease_quantity($_COOKIE[$product_key]);
		setcookie($product_key, $new_value, time()+3600*24);
	}
	$location_string = 'Location: ' . $root . '?view_cart_check_out=true';
	header($location_string);	
}


/* --- =list cart items for email content --- */
function list_cart_items_receipt($product_data, $email_content) {
	if(is_logged_in()) {
		$username = get_username();
		$item_prefix = $username . '_cart_item_';
		$total = 0;
		foreach ($_COOKIE as $key => $value) {
			if (strpos($key, $item_prefix) !== false) {
				$quantity = substr($value, -1); // get the qualtity number (last character)
				$quantity = (int)$quantity; // cast to an integer				
				$value = substr($value, 0, -3); // remove quantity suffix
				$price = get_item_price($product_data, $value);
				$price *= $quantity;
				$total = $total + $price;
				$email_content .= $value . ' x' . $quantity . ' - $' . $price . ' <br>';
			}
		}
		$email_content .= 'Total: $' . $total; 
	}
	return $email_content;
}


/* --- =get an item's price from it's name --- */
function get_item_price($product_data, $item_name) {
	foreach ($product_data as $key => $value) {
		if ($value['name'] == $item_name) {
			return $value['price'];
		}
	}
}


/* --- =remove item from cart --- */
function remove_item($item) {
	setcookie($item, null, -1);
	$location_string = 'Location: ' . $root . '?view_cart_check_out=true';
	header($location_string);
}

?>